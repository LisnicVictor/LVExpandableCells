//
//  LVOptionCell.swift
//  Pods
//
//  Created by Victor Lisnic on 9/18/17.
//
//

import UIKit


public protocol LVOption {
    var title : String? {get}
}

public protocol LVOptionCellDelegate : LVExpandableCellDelegate {
    func didSelect(cell:LVOptionPickerCell,option:LVOption)
}

public class LVOptionDefaultCell : UITableViewCell {
    func configure(with option:LVOption) {
        textLabel?.text = option.title
    }
}

public class LVOptionPickerCell: UITableViewCell , UITableViewDataSource, UITableViewDelegate, LVExpandableCellType {

    public var indexPath: IndexPath = IndexPath()
    public var expandDelegate: LVExpandableCellDelegate?
    public var height: CGFloat = UITableViewAutomaticDimension
    public var containingTableView : UITableView?

    public var options : [LVOption]?
    public var delegate : LVOptionCellDelegate?
    public var optionSelectedClosure : ( (_ option:LVOption) -> () )?

    public var selectedOption : LVOption? {
        didSet {
            configureWith(option: selectedOption!)
        }
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        tableView?.isHidden = true
    }



    lazy var tableView : UITableView? = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 40
        tableView.estimatedRowHeight = 40
        self.contentView.addSubview(tableView)
        self.contentView.bringSubview(toFront: tableView)
        tableView.setContentCompressionResistancePriority(UILayoutPriority.required, for: .vertical)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            tableView.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor)])
        tableView.reloadData()
        return tableView
    }()

    public func configureWith(option:LVOption) {
        textLabel?.text = option.title
    }

    public func expand(_ expand: Bool, animated: Bool) {
        switch expand {
        case true:
            showOptions()
        case false:
            hideOptions()
        }
        expandDelegate?.expanded(indexPath:indexPath, expanded: expand)
    }

    public func showOptions() {
        tableView?.isHidden = false
        guard let rowheight = tableView?.contentSize.height else {return}
        height = rowheight
    }

    public func hideOptions() {
        tableView?.isHidden = true
        height = 40
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        expand(selected, animated: animated)
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options?.count ?? 0
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let option = options?[indexPath.row]
        else { return UITableViewCell() }
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell") as? LVOptionDefaultCell ?? LVOptionDefaultCell(style:UITableViewCellStyle.default, reuseIdentifier: "optionCell")
        cell.configure(with: option)
        return cell
    }

    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let option = options?[indexPath.row] else {return}
        delegate?.didSelect(cell:self,option: option)
        optionSelectedClosure?(option)
        selectedOption = option
        expand(false, animated: true)
        setSelected(false, animated: true)
    }
}
