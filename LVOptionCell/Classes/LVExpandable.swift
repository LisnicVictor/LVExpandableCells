//
//  LVExpandable.swift
//  Pods
//
//  Created by Victor Lisnic on 9/19/17.
//
//

import Foundation

public typealias LVExpandableCell = UITableViewCell & LVExpandableCellType

public protocol LVExpandableCellDelegate {
    func expanded(indexPath:IndexPath, expanded:Bool)
}

public protocol LVExpandableCellType {
    var height : CGFloat {get}
    var indexPath : IndexPath {get set}
    var expandDelegate : LVExpandableCellDelegate? {get set}
    var containingTableView : UITableView? {get set}
    func expand(_ expand:Bool, animated:Bool)
}

public protocol LVExpandableHandling : class {
    var tableView : UITableView {get}
    var expandedIndexPaths : [IndexPath] {get set}
    func setup(cell:LVExpandableCell, indexPath:IndexPath, closure:((LVExpandableCell)->())?)
}
public extension LVExpandableHandling {
    func setup(cell:LVExpandableCell, indexPath:IndexPath, closure:((LVExpandableCell)->())?) {
        closure?(cell)
        cell.expand(expandedIndexPaths.contains(indexPath), animated: false)
    }
}

public extension LVExpandableHandling where Self : LVExpandableCellDelegate {
    func expanded(indexPath:IndexPath, expanded:Bool) {
        switch  expanded {
        case true where !expandedIndexPaths.contains(indexPath):
            expandedIndexPaths.append(indexPath)
        case false where expandedIndexPaths.contains(indexPath):
            let index = expandedIndexPaths.index(of:indexPath)!
            expandedIndexPaths.remove(at: index)
        default:
            break
        }

        tableView.beginUpdates()
        print("updated")
        tableView.endUpdates()
    }
}

