
//
//  LVDatePickerCell.swift
//  LVOptionCell
//
//  Created by Victor Lisnic on 9/25/17.
//

import UIKit

public class LVDatePickerCell: UITableViewCell , LVExpandableCellType {

    public var indexPath: IndexPath = IndexPath()
    public var height: CGFloat = 0.0
    public var expandDelegate: LVExpandableCellDelegate?
    public var containingTableView: UITableView?

    var expanded : Bool = false {
        didSet {
            expand(expanded, animated: false)
        }
    }

    public override func systemLayoutSizeFitting(_ targetSize: CGSize, withHorizontalFittingPriority horizontalFittingPriority: UILayoutPriority, verticalFittingPriority: UILayoutPriority) -> CGSize {
        let size = super.systemLayoutSizeFitting(targetSize, withHorizontalFittingPriority: horizontalFittingPriority, verticalFittingPriority: verticalFittingPriority)
        return CGSize(width: size.width, height: height)
    }

    public override func prepareForReuse() {
        super.prepareForReuse()
        expanded = false
    }

    public func expand(_ expand: Bool, animated: Bool) {
        height = expand ? 100 : 40
        if animated {
            expandDelegate?.expanded(indexPath: indexPath, expanded: expand)
        }
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
