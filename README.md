# LVOptionCell

[![CI Status](http://img.shields.io/travis/Lisnic_Victor/LVOptionCell.svg?style=flat)](https://travis-ci.org/Lisnic_Victor/LVOptionCell)
[![Version](https://img.shields.io/cocoapods/v/LVOptionCell.svg?style=flat)](http://cocoapods.org/pods/LVOptionCell)
[![License](https://img.shields.io/cocoapods/l/LVOptionCell.svg?style=flat)](http://cocoapods.org/pods/LVOptionCell)
[![Platform](https://img.shields.io/cocoapods/p/LVOptionCell.svg?style=flat)](http://cocoapods.org/pods/LVOptionCell)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LVOptionCell is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LVOptionCell'
```

## Author

Lisnic_Victor, gerasim.kisslin@gmail.com

## License

LVOptionCell is available under the MIT license. See the LICENSE file for more info.
