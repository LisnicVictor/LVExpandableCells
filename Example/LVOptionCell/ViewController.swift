//
//  ViewController.swift
//  LVOptionCell
//
//  Created by Lisnic_Victor on 09/18/2017.
//  Copyright (c) 2017 Lisnic_Victor. All rights reserved.
//

import UIKit
import LVOptionCell

enum Options{
    case first
    case second
    case third
}

extension Options : LVOption {
    var title : String? {
        switch self {
        case .first:
            return "first"
        case .second:
            return "second"
        case .third:
            return "third"
        }
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var table : UITableView!

    var expandedIndexPaths: [IndexPath] = []

    var tableView: UITableView {
        return table
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        table.rowHeight = UITableViewAutomaticDimension
        table.estimatedRowHeight = 40
    }
}

extension ViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if let cell = tableView.cellForRow(at: indexPath) as? LVExpandableCellType {
            cell.expand(!expandedIndexPaths.contains(indexPath), animated: true)
        }
    }
}

extension ViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LVDatePickerCell
        let k = 1 - CGFloat(indexPath.row)/255*10
        cell.backgroundColor = UIColor.init(red: k, green:k, blue:k, alpha: 1)
        cell.expandDelegate = self
        cell.indexPath = indexPath
        setup(cell: cell, indexPath: indexPath, closure: nil)
        return cell
    }
}

extension ViewController : LVExpandableHandling, LVExpandableCellDelegate {}

